namespace tech_test_payment_api.Models
{
    public enum EnumSatusSale
    {
        PagamentoAprovado,
        EnviadoParaTransportadora,
        Entregue,
        Cancelada,
        AguardandoPagamento
    }
}