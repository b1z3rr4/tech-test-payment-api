using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class Sale
    {
        public int Id { get; set; }
        public DateTime Data { get; set; }
        public EnumSatusSale Status { get; set; }
        public Seller Seller { get; set; }
        public List<Product> Products { get; set; }
    }
}