using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Contexts;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SalesController : ControllerBase
    {
            
        private readonly SalesContext _context;

        public SalesController(SalesContext context)
        {
            _context = context;
        }
        
        [HttpGet("ObterTodos")]
        public IActionResult ObterTodos()
        {
            var sales = _context.Sales.OrderBy(x => x.Data );
            return Ok(sales);
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var sale = _context.Sales.Find(id);
            
            if (sale == null)
                return NotFound();

            return Ok(sale);
        }

        [HttpPost]
        public IActionResult CriarVenda(Sale sale)
        {
            sale.Status = EnumSatusSale.AguardandoPagamento;
            sale.Data = DateTime.Now;
            _context.Add(sale);
            _context.SaveChanges();
            return CreatedAtAction(nameof(ObterPorId), new { id = sale.Id }, sale);
        }

        [HttpPut]
        public IActionResult AtualizarVenda(int id, Sale sale)
        {
            var oldSale = _context.Sales.Find(id);
            
            if (oldSale == null)
                return NotFound();

            oldSale.Data = DateTime.Now;

            oldSale.Products = sale.Products;
            oldSale.Seller = sale.Seller;
            
            if (sale.Status == EnumSatusSale.PagamentoAprovado && oldSale.Status != EnumSatusSale.AguardandoPagamento)
                return BadRequest(new { Error = "Essa atualização de status não é permitida!" });
            
            if (sale.Status == EnumSatusSale.Cancelada && (oldSale.Status != EnumSatusSale.AguardandoPagamento && oldSale.Status != EnumSatusSale.PagamentoAprovado))
                return BadRequest(new { Error = "Essa atualização de status não é permitida!" });
            
            if (sale.Status == EnumSatusSale.EnviadoParaTransportadora && oldSale.Status != EnumSatusSale.PagamentoAprovado)
                return BadRequest(new { Error = "Essa atualização de status não é permitida!" });
            
            if (sale.Status == EnumSatusSale.Entregue && oldSale.Status != EnumSatusSale.EnviadoParaTransportadora)
                return BadRequest(new { Error = "Essa atualização de status não é permitida!" });

            oldSale.Status = sale.Status;

            _context.Sales.Update(oldSale);
            _context.SaveChanges();

            return Ok(oldSale);
        }
    }
}